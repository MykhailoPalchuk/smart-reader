//
//  ApiWord.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

struct ApiWord: Codable, Hashable {
  let definition: String?
  let partOfSpeech: String?
  let antonyms: [String]?
  let examples: [String]?
  let synonyms: [String]?
  let typeOf: [String]?
  let also: [String]?
  let entails: [String]?
  let hasCategories: [String]?
  let hasInstances: [String]?
  let hasMembers: [String]?
  let hasParts: [String]?
  let hasSubstances: [String]?
  let hasTypes: [String]?
  let hasUsages: [String]?
  let inCategory: [String]?
  let inRegion: [String]?
  let instanceOf: [String]?
  let memberOf: [String]?
  let partOf: [String]?
  let pertainsTo: [String]?
  let regionOf: [String]?
  let similarTo: [String]?
  let substanceOf: [String]?
  let usageOf: [String]?
  let derivation: [String]?
}

struct ApiWordEnvelope: Codable, Hashable {
  struct Pronunciation: Codable, Hashable {
    let all: String
  }

  let word: String
  let results: [ApiWord]
  let frequency: Double?
//  let pronunciation: Pronunciation?
}
