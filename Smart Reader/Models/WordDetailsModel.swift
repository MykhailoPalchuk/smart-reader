//
//  WordDetailsModel.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/5/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

struct WordDetailsModel: Codable, Hashable {
  let word: String
  let details: [ApiWord]
  let translation: String
}
