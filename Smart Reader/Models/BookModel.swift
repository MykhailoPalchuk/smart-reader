//
//  BookModel.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 2/24/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

struct BookModel: Codable {
  let id: UUID
  let title: String
  let author: String
  let language: String
  
  var paragraphs: [String]
}
