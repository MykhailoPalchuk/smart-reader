//
//  Translation.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

struct Translations: Codable, Hashable {
  struct Translation: Codable, Hashable {
    let translatedText: String
  }
  
  let translations: [Translation]
}

struct TranslationEnvelope: Codable, Hashable {
  let data: Translations
}
