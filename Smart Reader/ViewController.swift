//
//  ViewController.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 2/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  var picker: UIDocumentPickerViewController?
  var parser = EpubParser()

  @IBAction func openAction(_ sender: Any) {
    picker = UIDocumentPickerViewController(documentTypes: ["org.idpf.epub-container"], in: .import)
    picker?.delegate = self
    present(picker!, animated: true)
  }
}

extension ViewController: UIDocumentPickerDelegate {
  func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
    print("Selected files at urls: \(urls)")

    guard let book = parser.parseBook(at: urls[0]) else {
        return
    }

    let reader = BookReaderController(
        transitionStyle: .pageCurl,
        navigationOrientation: .horizontal,
        options: nil
    )
    reader.book = book
//        let dataSource = BookReaderDataSource()
//        let contentSize = CGSize(
//            width: view.frame.width - view.safeAreaInsets.left - view.safeAreaInsets.right - 40,
//            height: view.frame.height - view.safeAreaInsets.top - view.safeAreaInsets.bottom - 40
//        )
//        dataSource.setup(for: book, contentSize: contentSize)
//        reader.dataSource = dataSource
    navigationController?.pushViewController(reader, animated: true)
//        reader.setViewControllers([dataSource.firstViewController], direction: .forward, animated: false, completion: nil)
  }
}

let sampleText = """
Project Gutenberg's Dr. Jekyll and Mr. Hyde, by Robert Louis Stevenson
This eBook is for the use of anyone anywhere at no cost and with almost no restrictions whatsoever. You may copy it, give it away or re-use it under the terms of the Project Gutenberg License included with this eBook or online at www.gutenberg.net
Title: Dr. Jekyll and Mr. Hyde
Author: Robert Louis Stevenson
Posting Date: December 18, 2011 [EBook #42] Release Date: October, 1992 Last Updated: July 1, 2005
Language: English
*** START OF THIS PROJECT GUTENBERG EBOOK DR. JEKYLL AND MR. HYDE ***
[Editor's Note: It has been called to our attention that Project Gutenberg ebook #43 which is the same title as this, is much easier to read than file #42 which you have presently opened.]
1)
MR. UTTERSON the lawyer was a man of a rugged countenance, that was never lighted by a smile; cold, scanty and embarrassed in discourse; backward in sentiment; lean, long, dusty, dreary, and yet somehow lovable. At friendly meetings, and when the wine was to his taste, something eminently human beaconed from his eye; something indeed which never found its way into his talk, but which spoke not only in these silent symbols of the after-dinner face, but more often and loudly in the acts of his life. He was austere with himself; drank gin when he was alone, to mortify a taste for vintages; and though he enjoyed the theatre, had not crossed the doors of one for twenty years. But he had an approved tolerance for others; sometimes wondering, almost with envy, at the high pressure of spirits Project Gutenberg's Dr. Jekyll and Mr. Hyde, by Robert Louis Stevenson
This eBook is for the use of anyone anywhere at no cost and with almost no restrictions whatsoever. You may copy it, give it away or re-use it under the terms of the Project Gutenberg License included with this eBook or online at www.gutenberg.net
Title: Dr. Jekyll and Mr. Hyde
Author: Robert Louis Stevenson
Posting Date: December 18, 2011 [EBook #42] Release Date: October, 1992 Last Updated: July 1, 2005
Language: English
*** START OF THIS PROJECT GUTENBERG EBOOK DR. JEKYLL AND MR. HYDE ***
[Editor's Note: It has been called to our attention that Project Gutenberg ebook #43 which is the same title as this, is much easier to read than file #42 which you have presently opened.]
1)
MR. UTTERSON the lawyer was a man of a rugged countenance, that was never lighted by a smile; cold, scanty and embarrassed in discourse; backward in sentiment; lean, long, dusty, dreary, and yet somehow lovable. At friendly meetings, and when the wine was to his taste, something eminently human beaconed from his eye; something indeed which never found its way into his talk, but which spoke not only in these silent symbols of the after-dinner face, but more often and loudly in the acts of his life. He was austere with himself; drank gin when he was alone, to mortify a taste for vintages; and though he enjoyed the theatre, had not crossed the doors of one for twenty years. But he had an approved tolerance for others; sometimes wondering, almost with envy, at the high pressure of spirits1)
MR. UTTERSON the lawyer was a man of a rugged countenance, that was never lighted by a smile; cold, scanty and embarrassed in discourse; backward in sentiment; lean, long, dusty, dreary, and yet somehow lovable. At friendly meetings, and when the wine was to his taste, something eminently human beaconed from his eye; something indeed which never found its way into his talk, but which spoke not only in these silent symbols of the after-dinner face, but more often and loudly in the acts of his life. He was austere with himself; drank gin when he was alone, to mortify a taste for vintages; and though he enjoyed the theatre, had not crossed the doors of one for twenty years. But he had an approved tolerance for others; sometimes wondering, almost with envy, at the high pressure of spirits1)
MR. UTTERSON the lawyer was a man of a rugged countenance, that was never lighted by a smile; cold, scanty and embarrassed in discourse; backward in sentiment; lean, long, dusty, dreary, and yet somehow lovable. At friendly meetings, and when the wine was to his taste, something eminently human beaconed from his eye; something indeed which never found its way into his talk, but which spoke not only in these silent symbols of the after-dinner face, but more often and loudly in the acts of his life. He was austere with himself; drank gin when he was alone, to mortify a taste for vintages; and though he enjoyed the theatre, had not crossed the doors of one for twenty years. But he had an approved tolerance for others; sometimes wondering, almost with envy, at the high pressure of spirits
"""
