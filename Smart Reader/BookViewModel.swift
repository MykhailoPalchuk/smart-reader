//
//  BookViewModel.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 2/25/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

struct BookViewModel {
  let bookId: UUID
  var fontSize: Int
  var pageCount: Int
  var currentPage: Int
  var pages: [NSAttributedString]
}
