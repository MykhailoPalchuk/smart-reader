//
//  EpubParser.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 2/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Zip
import SWXMLHash
import SwiftSoup

typealias BookMetadata = (title: String, author: String, language: String, content: [URL])

class EpubParser {
  func parseBook(at url: URL) -> BookModel? {
    if let metadata = getMetadata(from: url) {
      var book = BookModel(id: UUID(),
                           title: metadata.title,
                           author: metadata.author,
                           language: metadata.language,
                           paragraphs: [String]())
      for chapter in metadata.content {
        do {
          let htmlStr = try String(contentsOf: chapter, encoding: .utf8)
          let document: Document = try SwiftSoup.parse(htmlStr)
          let p = try document.select("p")
          book.paragraphs = try p.map { try $0.text() }
          return book
        } catch {
          print("Error parsing html content: \(error.localizedDescription)")
        }
      }
    }
    return nil
  }
    
  func getMetadata(from url: URL) -> BookMetadata? {
    Zip.addCustomFileExtension("epub")
    let manager = FileManager.default
    if let documentsUrl = manager.urls(for: .documentDirectory, in: .userDomainMask).last,
      var folderName = url.pathComponents.last {
      if folderName.hasSuffix(".epub") {
        folderName = String(folderName[..<folderName.index(folderName.endIndex, offsetBy: -5)])
      }
      let folderURL = documentsUrl.appendingPathComponent(folderName, isDirectory: true)
      do {
        try? manager.createDirectory(at: folderURL, withIntermediateDirectories: false, attributes: nil)
        try Zip.unzipFile(url, destination: folderURL, overwrite: true, password: nil)
        print("File successfully unarchived")
        return parseMetadata(atUrl: folderURL)
      } catch {
        print("Error creating directory: \(error)")
      }
    }
    return nil
  }
    
  private func parseMetadata(atUrl url: URL) -> BookMetadata? {
    let containerFileUrl = url.appendingPathComponent("META-INF")
        .appendingPathComponent("container.xml")
    do {
      let data = try Data(contentsOf: containerFileUrl)
      let xml = SWXMLHash.parse(data)
      if let rootfileElement = xml["container"]["rootfiles"]["rootfile"][0].element,
        let fullPath = rootfileElement.attribute(by: "full-path")?.text {

        print("Content path: \(fullPath)")

        let contentFileUrl = url.appendingPathComponent(fullPath)
        let contentData = try Data(contentsOf: contentFileUrl)
        let contentXml = SWXMLHash.parse(contentData)

        // Get book metadata
        var title = ""
        var author = ""
        var language = ""
        if let metadata = contentXml["package"]["metadata"].element {
        print("Metadata: \(metadata)")
        for item in metadata.children {
          if let element = item as? SWXMLHash.XMLElement {
              if element.name == "dc:creator" {
                author = element.text
              }
              if element.name == "dc:title" {
                title = element.text
              }
              if element.name == "dc:language" {
                language = element.text
              }
            }
          }
        }

        if let manifest = contentXml["package"]["manifest"].element {
          print("Manifest : \(manifest)")
          var items: [String: String] = [:]
          for item in manifest.children {
            if let element = item as? SWXMLHash.XMLElement {
              if let id = element.attribute(by: "id")?.text,
                let href = element.attribute(by: "href")?.text {
                items[id] = href
              }
            }
          }
          print(items)

          if let spine = contentXml["package"]["spine"].element {
            print("Spine: \(spine)")
            var chapters: [String] = []
            for item in spine.children {
              if let element = item as? SWXMLHash.XMLElement,
                let id = element.attribute(by: "idref")?.text {
                chapters.append(id)
              }
            }
            print(chapters)
            let chaptersFolderUrl = contentFileUrl.deletingLastPathComponent()
            let chaptersUrls: [URL] = chapters.map { items[$0]! }.map { chaptersFolderUrl.appendingPathComponent($0) }
            print(chaptersUrls)
            return (title, author, language, chaptersUrls)
          }
        }
      }
    } catch {
      print(error)
    }
    return nil
  }
}
