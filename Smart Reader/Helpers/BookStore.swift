//
//  BookStore.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 4/2/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

class BookStore {

  // MARK: - Constants

  private let booksFileName = "books"

  // MARK: - Properties

  private var fileUrl: URL? {
    return FileManager.default
        .urls(for: .documentDirectory, in: .userDomainMask)
        .first?
        .appendingPathComponent(booksFileName)
  }

  // MARK: - Methods

  func getBooks() -> [BookModel]? {
    if let fileUrl = fileUrl {
        do {
            let data = try Data(contentsOf: fileUrl)
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            let books = try decoder.decode([BookModel].self, from: data)
            return books
        } catch {
            print("Error reading cache: \(error)")
        }
    }
      return nil
  }

  func saveBooks(_ books: [BookModel]) {
    let encoder = JSONEncoder()
    encoder.dateEncodingStrategy = .iso8601
    do {
        let json = try encoder.encode(books)
        if let fileUrl = fileUrl {
            try json.write(to: fileUrl)
        }
    } catch {
        print("Error saving cache: \(error)")
    }
  }
}
