//
//  TextManager.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 2/25/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

class TextManager {
  static func slice(text: NSAttributedString, width: CGFloat, pageHeight: CGFloat) -> [NSAttributedString] {
      var parts: [NSAttributedString] = []
      let framesetter = CTFramesetterCreateWithAttributedString(text as CFAttributedString)
      var textPos = 0
      while textPos < text.length {
          let frame = CGRect(x: 0, y: 0, width: width, height: pageHeight)
          let path = CGMutablePath()
          path.addRect(CGRect(origin: .zero, size: frame.size))
          let ctFrame = CTFramesetterCreateFrame(framesetter, CFRangeMake(textPos, 0), path, nil)
          let frameRange = CTFrameGetVisibleStringRange(ctFrame)
          parts.append(text.attributedSubstring(from: NSMakeRange(frameRange.location, frameRange.length)))
          textPos += frameRange.length
      }
      return parts
  }
}
