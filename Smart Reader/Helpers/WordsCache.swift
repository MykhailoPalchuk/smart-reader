//
//  WordsCache.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/5/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

final class WordsCache {
  static let shared = WordsCache()

  private init() {
    words = getWords() ?? []
  }

  private(set) var words: [WordDetailsModel] = []

  // MARK: - Constants

  private let wordsFileName = "words"

  // MARK: - Properties

  private var fileUrl: URL? {
    return FileManager.default
      .urls(for: .documentDirectory, in: .userDomainMask)
      .first?
      .appendingPathComponent(wordsFileName)
  }

  // MARK: - Methods

  func saveWord(_ word: WordDetailsModel) {
    if words.firstIndex(of: word) == nil {
      words.append(word)
      saveWords(words)
    }
  }

  private func getWords() -> [WordDetailsModel]? {
    if let fileUrl = fileUrl {
      do {
        let data = try Data(contentsOf: fileUrl)
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        let words = try decoder.decode([WordDetailsModel].self, from: data)
        return words
      } catch {
        print("Error reading cache: \(error)")
      }
    }
    return nil
  }

  private func saveWords(_ words: [WordDetailsModel]) {
    let encoder = JSONEncoder()
    encoder.dateEncodingStrategy = .iso8601
    do {
      let json = try encoder.encode(words)
      if let fileUrl = fileUrl {
        try json.write(to: fileUrl)
      }
    } catch {
      print("Error saving cache: \(error)")
    }
  }
}
