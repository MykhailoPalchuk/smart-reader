//
//  AppDelegate.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 2/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?


  func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {

    let window = UIWindow()
    RootFlow.show(in: window)
    window.makeKeyAndVisible()
    self.window = window

    return true
  }
}

