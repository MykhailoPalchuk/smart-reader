//
//  NetworkProvider.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Moya
import Result
import UIKit

enum NetworkProviderError: LocalizedError {
  case moya(MoyaError)
  case apiError(envelope: ApiErrorEnvelope, statusCode: Int)
  case statusCodeError(error: Error, statusCode: Int)
  case parsingError(error: Error, statusCode: Int)
  case unknownError

  var errorDescription: String? {
    switch self {
    case let .moya(error):
      return error.localizedDescription

    case let .apiError(errorEnvelope, _):
      return errorEnvelope.errors.map { $0.message }.joined(separator: "\n")

    case let .statusCodeError(error, _):
      return error.localizedDescription

    case let .parsingError(error, _):
      return error.localizedDescription

    case .unknownError:
      return "Unknown error occured"
    }
  }
}

final class NetworkProvider<T: NetworkTarget> {
  let provider: MoyaProvider<T>

  init() {
    let endpointClosure: (T) -> Moya.Endpoint = { target in
      let endpoint = MoyaProvider<T>.defaultEndpointMapping(for: target)

      func endpointAdding(token: String) -> Endpoint {
        return endpoint.adding(newHTTPHeaderFields: ["Authorization": "Bearer \(token)"])
      }

      switch target.authType {
      case let .accessToken(token):
        return endpointAdding(token: token)

      default:
        return endpoint
      }
    }

    let networkActivityPlugin = NetworkActivityPlugin { (change, _) in
      DispatchQueue.main.async {
        switch change {
        case .began:
          NetworkActivity.shared.increaseCounter()

        case .ended:
          NetworkActivity.shared.decreaseCounter()
        }
      }
    }

    provider = MoyaProvider<T>(
      endpointClosure: endpointClosure,
      requestClosure: MoyaProvider<T>.defaultRequestMapping,
      stubClosure: { _ in StubBehavior.never },
      callbackQueue: DispatchQueue.main,
      manager: MoyaProvider<T>.defaultAlamofireManager(),
      plugins: [NetworkLoggerPlugin(verbose: true, cURL: true), networkActivityPlugin],
      trackInflights: false
    )
  }

  @discardableResult
  func request(
    target: T,
    completion: @escaping (Result<Response, NetworkProviderError>) -> Void
    ) -> Disposable {
    var cancellable: Cancellable?
    requestQueue.async {
      requestSemaphore.wait()

      if !target.isCritical {
        requestSemaphore.signal()
      }

      cancellable = self.provider.request(target) { result in
        switch result {
        case let .success(response):
          if let errorEnvelope = try? response.map(ApiErrorEnvelope.self) {
            if !errorEnvelope.errors.isEmpty {
              completion(.failure(.apiError(envelope: errorEnvelope, statusCode: response.statusCode)))
            } else {
              completion(.success(response))
            }
          } else {
            do {
              _ = try response.filterSuccessfulStatusCodes()
              completion(.success(response))

            } catch {
              completion(.failure(.statusCodeError(error: error, statusCode: response.statusCode)))
            }
          }

        case let .failure(error):
          if case let .underlying(nsError as NSError, _) = error, nsError.code == -999 {
            print("Network request cancelled.")

          } else {
            completion(.failure(.moya(error)))
          }
        }

        if target.isCritical {
          requestSemaphore.signal()
        }
      }
    }

    return Disposable {
      cancellable?.cancel()
    }
  }

  @discardableResult
  func request<Model: Hashable & Codable>(
    target: T,
    mapTo: Model.Type,
    completion: @escaping (Result<Model, NetworkProviderError>) -> Void
    ) -> Disposable {

    return self.request(target: target) { result in
      switch result {
      case let .success(response):
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .custom { decoder -> Date in
          let container = try decoder.singleValueContainer()
          let rawDate = try container.decode(String.self)

          let formatter = DateFormatter()
          formatter.calendar = Calendar(identifier: .iso8601)
          formatter.locale = Locale(identifier: "en_US_POSIX")
          formatter.timeZone = TimeZone(secondsFromGMT: 0)
          formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
          if let date = formatter.date(from: rawDate) {
            return date
          } else {
            throw InvalidDateError()
          }
        }
        do {
          let result = try response.map(Model.self, using: decoder)
          completion(.success(result))
        } catch {
          completion(.failure(.parsingError(error: error, statusCode: response.statusCode)))
        }

      case let .failure(error):
        completion(.failure(error))
      }
    }
  }
}

private struct InvalidDateError: Error { }

private let requestQueue = DispatchQueue(label: "networkprovider.requests", qos: .userInitiated)
private let requestSemaphore = DispatchSemaphore(value: 1)
