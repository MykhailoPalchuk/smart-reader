//
//  NetworkTarget.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Moya

protocol NetworkTarget: TargetType {
  var authType: NetworkAuthType { get }
  var isCritical: Bool { get }
}

extension NetworkTarget {
  var baseURL: URL {
    return googleAPIURL
  }

  var sampleData: Data {
    return Data()
  }

  var authType: NetworkAuthType {
    return .none
  }

  var validationType: Moya.ValidationType {
    return Moya.ValidationType.none
  }

  var isCritical: Bool {
    return false
  }
}
