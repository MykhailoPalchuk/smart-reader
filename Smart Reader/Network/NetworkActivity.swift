//
//  NetworkActivity.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

final class NetworkActivity {
  static let shared = NetworkActivity()

  private var counter = 0

  private init() { }

  func increaseCounter() {
    counter += 1
    updateActivityIfNeeded()
  }

  func decreaseCounter() {
    counter -= 1
    updateActivityIfNeeded()
  }

  private func updateActivityIfNeeded() {
    UIApplication.shared.isNetworkActivityIndicatorVisible = counter != 0
  }
}
