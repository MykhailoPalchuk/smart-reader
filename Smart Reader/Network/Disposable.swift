//
//  Disposable.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

final class Disposable {
  let dispose: () -> Void

  init(dispose: @escaping () -> Void) {
    self.dispose = dispose
  }
}
