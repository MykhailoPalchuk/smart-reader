//
//  ApiError.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

struct ApiErrorEnvelope: Hashable, Codable {
  struct ApiError: Hashable, Codable {
    let message: String
  }

  var errors: [ApiError]
}
