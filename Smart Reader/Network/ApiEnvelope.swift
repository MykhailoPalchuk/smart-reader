//
//  ApiEnvelope.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

struct ApiEnvelope<T: Hashable & Codable>: Hashable, Codable {
  var data: T
}
