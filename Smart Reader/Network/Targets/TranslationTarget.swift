//
//  TranslationTarget.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Moya

struct TranslationTarget {
  let text: String
  let targetLanguage: String
}

extension TranslationTarget: NetworkTarget {
  var authType: NetworkAuthType {
    return .none
  }

  var path: String {
    return ""
  }

  var method: Method {
    return .post
  }

  var task: Task {
    let parameters: [String: Any] = ["key": googleAPIKey, "q": text, "target": targetLanguage]
    return Task.requestParameters(parameters: parameters, encoding: URLEncoding())
  }

  var headers: [String: String]? {
    return nil
  }
}
