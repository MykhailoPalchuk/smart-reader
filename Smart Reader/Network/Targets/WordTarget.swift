//
//  WordTarget.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Moya

enum WordTarget {
  case word(String)
  case synonyms(word: String)
  case antonyms(word: String)
  case examples(word: String)
}

extension WordTarget: NetworkTarget {
  var baseURL: URL {
    return wordsAPIURL
  }

  var authType: NetworkAuthType {
    return .none
  }

  var path: String {
    switch self {
    case let .word(word):
      return word

    case let .synonyms(word: word):
      return "\(word)/synonyms"

    case let .antonyms(word: word):
      return "\(word)/antonyms"

    case let .examples(word):
      return "\(word)/examples"
    }
  }

  var method: Method {
    return .get
  }

  var task: Task {
    return Task.requestPlain
  }

  var headers: [String: String]? {
    return [
      "X-RapidAPI-Key": wordsAPIKey,
      "X-Mashape-Host": wordsAPIHost
    ]
  }
}
