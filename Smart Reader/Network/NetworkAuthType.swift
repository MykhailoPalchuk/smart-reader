//
//  NetworkAuthType.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

enum NetworkAuthType {
  case none
  case accessToken(String)
}
