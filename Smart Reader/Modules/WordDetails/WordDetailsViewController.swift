//
//  WordDetailsViewController.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/5/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

final class WordDetailsViewController: UIViewController {
  let word: String
  let details: [ApiWord]

  init(word: String, details: [ApiWord]) {
    self.word = word
    self.details = details
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }

  private func setup() {
    view.backgroundColor = .white

    let closeButton = UIButton()
    closeButton.setImage(#imageLiteral(resourceName: "close"), for: .normal)
    closeButton.addTarget(
      self,
      action: #selector(handleCloseButtonTap),
      for: .touchUpInside
    )
    closeButton.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(closeButton)
    NSLayoutConstraint.activate([
      closeButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
      closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8),
      closeButton.widthAnchor.constraint(equalToConstant: 24),
      closeButton.heightAnchor.constraint(equalToConstant: 24)
    ])

    let wordLabel = UILabel()
    wordLabel.text = word
    wordLabel.textAlignment = .center
    wordLabel.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
    wordLabel.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(wordLabel)
    NSLayoutConstraint.activate([
      wordLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      wordLabel.topAnchor.constraint(equalTo: closeButton.bottomAnchor, constant: 8)
    ])

    let textView = UITextView()
    textView.text = details
      .enumerated()
      .map(makeText)
      .joined(separator: "\n")
    textView.isEditable = false
    textView.font = UIFont.systemFont(ofSize: 16)
    textView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(textView)
    NSLayoutConstraint.activate([
      textView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
      textView.topAnchor.constraint(equalTo: wordLabel.bottomAnchor, constant: 8),
      textView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
      textView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16)
    ])
  }

  private func makeText(index: Int, from word: ApiWord) -> String {
    var result = "\(index + 1)."
    if let partOfSpeech = word.partOfSpeech {
      result.append("\(partOfSpeech)\n")
    }
    if let definition = word.definition {
      result.append("- \(definition)\n")
    }
    if let examples = word.examples {
      result.append("Examples: \(examples.joined(separator: ", "))\n")
    }
    if let synonyms = word.synonyms {
      result.append("Synonyms: \(synonyms.joined(separator: ", "))\n")
    }
    if let antonyms = word.antonyms {
      result.append("Antonyms: \(antonyms.joined(separator: ", "))\n")
    }
    if let typeOf = word.typeOf {
      result.append("Is type of \(typeOf.joined(separator: ", "))\n")
    }
    if let hasTypes = word.hasTypes {
      result.append("Has types \(hasTypes.joined(separator: ", "))\n")
    }
    if let hasParts = word.hasParts {
      result.append("Has parts \(hasParts.joined(separator: ", "))\n")
    }
    if let partOf = word.partOf {
      result.append("Part of \(partOf.joined(separator: ", "))\n")
    }
    return result
  }

  @objc private func handleCloseButtonTap() {
    dismiss(animated: true, completion: nil)
  }
}
