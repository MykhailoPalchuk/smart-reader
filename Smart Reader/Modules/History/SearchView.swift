//
//  SearchView.swift
//  Egg
//
//  Created by Евгений Матвиенко on 2/18/19.
//  Copyright © 2019 Egg. All rights reserved.
//

import UIKit

private struct State {
  var isEditing: Bool
}

final class SearchView: UIView, NibInstantiatable {
  var observer: ((String) -> Void)?
  var isEditingObserver: ((Bool) -> Void)?

  @IBOutlet private var stackView: UIStackView!
  @IBOutlet private var searchContainerView: UIView!
  @IBOutlet private var searchTextField: UITextField!
  @IBOutlet private var heightConstraint: NSLayoutConstraint!

  private var state = State(
    isEditing: false
  )
  private var searchText: String {
    return (searchTextField.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
  }
  private var previousNotifiedSearchText = ""
  private lazy var cancelButton: UIButton = {
    let button = UIButton()
    button.setTitle("Cancel", for: .normal)
    button.setTitleColor(#colorLiteral(red: 0.1294117647, green: 0.1647058824, blue: 0.1921568627, alpha: 0.7), for: .normal)
    button.setTitleColor(#colorLiteral(red: 0.1294117647, green: 0.1647058824, blue: 0.1921568627, alpha: 0.3), for: .highlighted)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .medium)
    button.contentEdgeInsets.left = 10
    button.addTarget(
      self,
      action: #selector(SearchView.handleCancelTap),
      for: .touchUpInside
    )
    button.setContentHuggingPriority(UILayoutPriority(999), for: .horizontal)
    return button
  }()
  var otherButtonView: UIView? {
    didSet {
      renderSearchingState()
    }
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    setup()
    renderSearchingState()
  }

  func set(height: CGFloat) {
    heightConstraint.constant = height
  }

  func setBackgroundColor(_ color: UIColor) {
    searchContainerView.backgroundColor = color
  }

  func setPlaceholderText(_ attributedString: NSAttributedString) {
    searchTextField.attributedPlaceholder = attributedString
  }

  func startEditing() {
    searchTextField.becomeFirstResponder()
  }

  private func setup() {
    searchContainerView.addGestureRecognizer(
      UITapGestureRecognizer(
        target: self,
        action: #selector(SearchView.handleSearchContainerTap)
      )
    )

    setPlaceholderText(NSAttributedString(
      string: "Search",
      attributes: [
        .font: UIFont.systemFont(ofSize: 15, weight: .semibold),
        .foregroundColor: #colorLiteral(red: 0.6078431373, green: 0.6078431373, blue: 0.6078431373, alpha: 1)
      ]
    ))
    searchTextField.delegate = self
    searchTextField.addTarget(
      self,
      action: #selector(SearchView.handleTextFieldValueChange),
      for: .editingChanged
    )
  }

  @objc private func handleSearchContainerTap() {
    searchTextField.becomeFirstResponder()
  }

  @objc private func handleCancelTap() {
    if searchText != "" {
      searchTextField.text = ""
      notifyObserver()
    }
    searchTextField.resignFirstResponder()
    renderSearchingState()
  }

  @objc private func handleTextFieldValueChange() {
    notifyObserver()
  }

  private func renderSearchingState() {
    let isSearching = state.isEditing || !searchText.isEmpty

    UIView.performWithoutAnimation {
      if self.stackView.arrangedSubviews.count > 1 {
        self.stackView.arrangedSubviews[1].removeFromSuperview()
      }
      if isSearching {
        self.stackView.addArrangedSubview(self.cancelButton)

      } else if let otherButtonView = self.otherButtonView {
        self.stackView.addArrangedSubview(otherButtonView)
      }
    }
  }

  private func notifyObserver() {
    let searchText = self.searchText
    if searchText != previousNotifiedSearchText {
      observer?(searchText)
      previousNotifiedSearchText = searchText
    }
  }
}

extension SearchView: UITextFieldDelegate {
  func textFieldDidBeginEditing(_ textField: UITextField) {
    state.isEditing = true
    isEditingObserver?(true)
    renderSearchingState()
  }

  func textFieldDidEndEditing(_ textField: UITextField) {
    state.isEditing = false
    isEditingObserver?(false)
    renderSearchingState()
  }
}
