//
//  HistoryTableViewCell.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/5/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

final class HistoryTableViewCell: UITableViewCell, NibInstantiatable, ReusableCell {

  @IBOutlet private weak var wordLabel: UILabel!
  @IBOutlet private weak var translationLabel: UILabel!

  func render(word: String, translation: String) {
    wordLabel.text = word
    translationLabel.text = translation
  }
}
