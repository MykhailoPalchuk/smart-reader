//
//  HistoryViewController.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/5/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

private struct State {
  var searchText: String = ""

  var isSearching: Bool {
    return !searchText.isEmpty
  }
}

final class HistoryViewController: UIViewController {
  var wordTap: ((WordDetailsModel) -> Void)?

  private var state = State()
  private var words: [WordDetailsModel] {
    let words = WordsCache.shared.words
    return state.isSearching ?
      words.filter({ $0.word.lowercased().starts(with: state.searchText) })
      :
      words
  }

  private lazy var searchView = SearchView.makeNibInstance()
  private lazy var tableView: UITableView = {
    let view = UITableView(frame: .zero, style: .plain)
    view.separatorStyle = .none
    view.dataSource = self
    view.delegate = self
    view.register(
      HistoryTableViewCell.nib,
      forCellReuseIdentifier: HistoryTableViewCell.reuseIdentifier
    )
    view.panGestureRecognizer.addTarget(self, action: #selector(handlePanGesture))
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  private lazy var searchDebouncer: Debouncer = {
    return Debouncer(delay: 0.6) { [weak self] in
      self?.tableView.reloadData()
    }
  }()

  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    tableView.reloadData()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    tabBarController?.title = "Words history"
    tabBarController?.navigationItem.rightBarButtonItem = nil
  }

  private func setup() {
    view.backgroundColor = .white

    searchView.observer = { [weak self] searchText in
      self?.state.searchText = searchText.lowercased()
      self?.searchDebouncer.call()
    }
    searchView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(searchView)
    NSLayoutConstraint.activate([
      searchView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      searchView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      searchView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
    ])

    view.addSubview(tableView)
    NSLayoutConstraint.activate([
      tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      tableView.topAnchor.constraint(equalTo: searchView.bottomAnchor),
      tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
    ])
  }

  @objc private func handlePanGesture() {
    searchView.endEditing(false)
  }
}

extension HistoryViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return words.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withType: HistoryTableViewCell.self, for: indexPath)
    let word = words[indexPath.row]
    cell.render(word: word.word, translation: word.translation)
    return cell
  }
}

extension HistoryViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    wordTap?(words[indexPath.row])
    tableView.deselectRow(at: indexPath, animated: true)
  }
}
