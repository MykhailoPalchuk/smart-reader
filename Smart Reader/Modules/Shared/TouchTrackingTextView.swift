//
//  TouchTrackingTextView.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

class TouchTreckingTextView: UITextView {
  var touchAction: ((UITouch) -> Void)?

  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesEnded(touches, with: event)
    touchAction?(touches[touches.index(touches.startIndex, offsetBy: touches.count - 1)])
  }
}
