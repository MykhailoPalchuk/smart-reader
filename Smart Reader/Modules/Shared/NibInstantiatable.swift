//
//  NibInstantiatable.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

protocol NibInstantiatable {
  static func makeNibInstance() -> Self
}

extension NibInstantiatable where Self: UIView {
  static var nib: UINib {
    return UINib(
      nibName: String(describing: Self.self),
      bundle: Bundle(for: Self.self)
    )
  }

  static func makeNibInstance() -> Self {
    guard let view = nib.instantiate(withOwner: nil, options: nil).first as? Self else {
      fatalError("Could not instantiate view from nib")
    }
    return view
  }
}
