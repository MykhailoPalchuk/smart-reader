//
//  ImageResize.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/5/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

extension UIImage {
  func resizeAspectFit(bounds: CGSize) -> UIImage {
    let ratio = min(bounds.width / size.width, bounds.height / size.height)
    let newImageSize = CGSize(width: size.width * ratio, height: size.height * ratio)

    UIGraphicsBeginImageContextWithOptions(bounds, false, UIScreen.main.scale)
    defer { UIGraphicsEndImageContext() }

    self.draw(in:
      CGRect(
        origin: CGPoint(
          x: (bounds.width - newImageSize.width) / 2,
          y: (bounds.height - newImageSize.height) / 2
        ),
        size: newImageSize
      )
    )
    return UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
  }

  func resizeAspectFill(bounds: CGSize) -> UIImage {
    let ratio = max(bounds.width / size.width, bounds.height / size.height)
    let newImageSize = CGSize(width: size.width * ratio, height: size.height * ratio)

    UIGraphicsBeginImageContextWithOptions(bounds, false, UIScreen.main.scale)
    defer { UIGraphicsEndImageContext() }

    self.draw(in:
      CGRect(
        origin: CGPoint(
          x: (bounds.width - newImageSize.width) / 2,
          y: (bounds.height - newImageSize.height) / 2
        ),
        size: newImageSize
      )
    )
    return UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
  }
}
