//
//  Debouncer.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 4/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

final class Debouncer: NSObject {
  private let callback: () -> Void
  private let delay: Double
  private weak var timer: Timer?

  init(delay: Double, callback: @escaping () -> Void) {
    self.delay = delay
    self.callback = callback
  }

  func call() {
    timer?.invalidate()
    let nextTimer = Timer.scheduledTimer(
      timeInterval: delay,
      target: self,
      selector: #selector(Debouncer.fireNow),
      userInfo: nil,
      repeats: false
    )
    timer = nextTimer
  }

  @objc private func fireNow() {
    callback()
  }
}
