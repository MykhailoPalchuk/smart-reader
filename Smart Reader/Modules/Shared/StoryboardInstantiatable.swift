//
//  StoryboardInstantiatable.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 3/9/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

protocol StoryboardInstantiatable {
  static func makeStoryboardInstance() -> Self
}

extension StoryboardInstantiatable where Self: UIViewController {
  static func makeStoryboardInstance() -> Self {
      let storyboard = UIStoryboard(
          name: String(describing: Self.self),
          bundle: Bundle(for: Self.self)
      )
      guard let viewController = storyboard.instantiateInitialViewController() as? Self else {
          fatalError("Could not instantiate view controller from storyboard")
      }
      return viewController
  }
}
