//
//  Array+Rearranged.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/10/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

extension Array {
  func rearranged() -> Array {
    var copy = self
    for i in 0..<count {
      let element = copy.remove(at: i)
      copy.insert(element, at: .random(in: 0..<count))
    }
    return copy
  }
}
