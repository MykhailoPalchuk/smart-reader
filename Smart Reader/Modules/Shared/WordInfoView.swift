//
//  WordInfoView.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/4/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

final class WordInfoView: UIView, NibInstantiatable {
  var topButtonTap: (() -> Void)?
  var bottomButtonTap: (() -> Void)?

  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var topButton: UIButton!
  @IBOutlet private weak var textView: UITextView!
  @IBOutlet private weak var bottomButton: UIButton!
  @IBOutlet private weak var bottomLabel: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib()

    setup()
  }

  func renderTranslation(_ text: String, targetLanguage: String) {
    titleLabel.text = "Translation"
    topButton.setTitle(nil, for: .normal)
    textView.text = text
    bottomButton.setTitle(targetLanguage, for: .normal)
    bottomLabel.text = "Translated to"
  }

  func renderDefinition(_ definition: String, word: String) {
    titleLabel.text = word.capitalized
    topButton.setTitle("More", for: .normal)
    textView.text = definition
    bottomButton.setTitle("Google it", for: .normal)
    bottomLabel.text = ""
  }

  private func setup() {
    backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    layer.cornerRadius = 8

    textView.layer.cornerRadius = 9

    topButton.addTarget(
      self,
      action: #selector(handleTopButtonTap),
      for: .touchUpInside
    )
    bottomButton.addTarget(
      self,
      action: #selector(handleBottomButtonTap),
      for: .touchUpInside
    )
  }

  @objc private func handleTopButtonTap() {
    topButtonTap?()
  }

  @objc private func handleBottomButtonTap() {
    bottomButtonTap?()
  }
}
