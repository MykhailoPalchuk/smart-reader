//
//  WordInfoContainerView.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/5/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

final class WordInfoContainerView: UIView {
  var presentDetails: (([ApiWord]) -> Void)?

  private let scrollView = UIScrollView()
  private let stackView = UIStackView()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }


  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  func render(word: String, completion: @escaping (Error?) -> Void) {
    fetchTranslation(word) { [weak self] error, translation in
      guard let translation = translation else {
        completion(error)
        return
      }

      self?.fetchDefinition(word) { [weak self] _, definition in
        WordsCache.shared.saveWord(WordDetailsModel(
          word: word,
          details: definition ?? [],
          translation: translation
        ))
        self?.showWordDetails(word: word, translation: translation, definitions: definition)
        completion(nil)
      }
    }
  }

  private func setup() {
    layer.shadowColor = UIColor.black.cgColor
    layer.shadowOpacity = 0.2
    layer.shadowOffset = CGSize(width: 0, height: 2)
    clipsToBounds = false

    scrollView.isPagingEnabled = true
    scrollView.showsHorizontalScrollIndicator = false
    scrollView.clipsToBounds = false
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(scrollView)
    NSLayoutConstraint.activate([
      scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
      scrollView.topAnchor.constraint(equalTo: topAnchor),
      scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
      scrollView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])

    stackView.axis = .horizontal
    stackView.spacing = 16
    stackView.clipsToBounds = false
    stackView.translatesAutoresizingMaskIntoConstraints = false

    scrollView.addSubview(stackView)
    NSLayoutConstraint.activate([
      stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
      stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
      stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
      stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
    ])
  }

  private func fetchTranslation(_ word: String, completion: @escaping (Error?, String?) -> Void) {
    if let cached = WordsCache.shared.words.first(where: { $0.word == word }) {
      completion(nil, cached.translation)
      return
    }

    let translationTarget = TranslationTarget(text: word, targetLanguage: "uk")
    NetworkProvider<TranslationTarget>().request(target: translationTarget, mapTo: TranslationEnvelope.self) { result in
      switch result {
      case let .success(translation):
        completion(nil, translation.data.translations.first?.translatedText)

      case let .failure(error):
        print("Could not translate word due to error: \(error.localizedDescription)")
        completion(error, nil)
      }
    }
  }

  private func fetchDefinition(_ word: String, completion: @escaping (Error?, [ApiWord]?) -> Void) {
    if let cached = WordsCache.shared.words.first(where: { $0.word == word }),
      cached.details.count > 0 {
      completion(nil, cached.details)
      return
    }

    let wordTarget = WordTarget.word(word)
    NetworkProvider<WordTarget>().request(target: wordTarget, mapTo: ApiWordEnvelope.self) { result in
      switch result {
      case let .success(word):
        completion(nil, word.results)

      case let .failure(error):
        print("Counld not get definition due to error: \(error.localizedDescription)")
        completion(error, nil)
      }
    }
  }

  private func showWordDetails(word: String, translation: String, definitions: [ApiWord]?) {
    if let definitions = definitions,
      let definition = definitions.first?.definition {
      let definitionView = WordInfoView.makeNibInstance()
      definitionView.renderDefinition(definition, word: word)
      definitionView.topButtonTap = { [weak self] in
        self?.presentDetails?(definitions)
      }
      definitionView.bottomButtonTap = {
        if let url = URL(string: "https://www.google.com/search?q=\(word)"),
          UIApplication.shared.canOpenURL(url) {
          UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
      }
      stackView.addArrangedSubview(definitionView)
    }

    let translationView = WordInfoView.makeNibInstance()
    translationView.renderTranslation(translation, targetLanguage: "Ukrainian")
    stackView.addArrangedSubview(translationView)
  }
}
