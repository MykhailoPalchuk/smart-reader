//
//  RootFlow.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 4/3/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

class RootFlow {
  static func show(in window: UIWindow) {
    let bookStorageController = BookStorageViewController()
    bookStorageController.tabBarItem = UITabBarItem(
      title: "Books",
      image: #imageLiteral(resourceName: "books").resizeAspectFit(bounds: CGSize(width: 32, height: 32)),
      selectedImage: nil
    )

    let learningController = LearningViewController()
    learningController.tabBarItem = UITabBarItem(
      title: "Learning",
      image: #imageLiteral(resourceName: "learning").resizeAspectFit(bounds: CGSize(width: 32, height: 32)),
      selectedImage: nil
    )

    let historyController = HistoryViewController()
    historyController.tabBarItem = UITabBarItem(
      title: "History",
      image: #imageLiteral(resourceName: "history").resizeAspectFit(bounds: CGSize(width: 32, height: 32)),
      selectedImage: nil
    )
    historyController.wordTap = { word in
      let detailsController = WordDetailsViewController(word: word.word, details: word.details)
      historyController.present(detailsController, animated: true, completion: nil)
    }

    let tabBarController = UITabBarController()
    tabBarController.setViewControllers(
      [bookStorageController, learningController, historyController],
      animated: true
    )

    let navigationController = UINavigationController(rootViewController: tabBarController)
    window.rootViewController = navigationController
  }
}
