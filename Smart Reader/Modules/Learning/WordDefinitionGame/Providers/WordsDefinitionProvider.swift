//
//  WordsDefinitionProvider.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/10/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

struct WordsDefinitionGamePack {
  let words: [String]
  let definitions: [String]
}

typealias AnswerPack = [(word: String, definition: String)]

struct VerifiedAnswer {
  let word: String
  let definition: String
  let isCorrect: Bool
}

protocol WordsDefinitionProvider {
  var score: Int { get }
  var results: [VerifiedAnswer] { get }
  
  func getNextDataPack(completion: @escaping (WordsDefinitionGamePack?) -> Void)
  func acceptAnswer(_ pack: AnswerPack)
}
