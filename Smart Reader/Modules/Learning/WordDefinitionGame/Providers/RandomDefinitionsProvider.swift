//
//  RandomDefinitionsProvider.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/10/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

final class RandomDefinitionsProvider: WordsDefinitionProvider {
  private(set) var score: Int = 0
  private(set) var results: [VerifiedAnswer] = []

  func getNextDataPack(completion: @escaping (WordsDefinitionGamePack?) -> Void) {
    completion(nil)
  }

  func acceptAnswer(_ pack: AnswerPack) {

  }
}
