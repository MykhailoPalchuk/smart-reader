//
//  CachedDefinitionsProvider.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/10/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import Foundation

final class CachedDefinitionsProvider: WordsDefinitionProvider {
  private(set) var score: Int = 0
  private(set) var results: [VerifiedAnswer] = []

  private lazy var packsCount: Int = {
    let maxPossibleCount = WordsCache.shared.words.count / 4
    return maxPossibleCount > 10 ? 10 : maxPossibleCount
  }()
  private var currentPackNumber = -1
  private lazy var packs: [WordsDefinitionGamePack] = {
    let words = WordsCache.shared.words
    var selectedWords = [WordDetailsModel]()
    while selectedWords.count < packsCount * 4 {
      let index = Int.random(in: 0..<words.count - 1)
      if selectedWords.firstIndex(of: words[index]) == nil {
        selectedWords.append(words[index])
      }
    }
    var packs = [WordsDefinitionGamePack]()
    for i in stride(from: 0, to: selectedWords.count, by: 4) {
      let models = selectedWords[i..<i + 4]
      packs.append(WordsDefinitionGamePack(
        words: models.map({ $0.word }),
        definitions: models.map(pickRandomDefinition)
      ))
    }
    return packs
  }()

  func getNextDataPack(completion: (WordsDefinitionGamePack?) -> Void) {
    currentPackNumber += 1
    if currentPackNumber < packsCount {
      completion(packs[currentPackNumber])
    } else {
      completion(nil)
    }
  }

  func acceptAnswer(_ answerPack: AnswerPack) {
    guard currentPackNumber < packs.count else {
      return
    }
    
    var answers = [VerifiedAnswer]()
    let pack = packs[currentPackNumber]
    for i in 0..<pack.words.count {
      if let answer = answerPack.first(where: { $0.0 == pack.words[i] }) {
        answers.append(VerifiedAnswer(
          word: pack.words[i],
          definition: pack.definitions[i],
          isCorrect: answer.definition == pack.definitions[i]
        ))
        if answer.definition == pack.definitions[i] {
          score += 1
        }
      } else {
        answers.append(VerifiedAnswer(
          word: pack.words[i],
          definition: pack.definitions[i],
          isCorrect: false
        ))
      }
    }
    results.append(contentsOf: answers)
  }

  private func pickRandomDefinition(_ model: WordDetailsModel) -> String {
    let index = Int.random(in: 0..<model.details.count)
    return model.details[index].definition ?? ""
  }
}
