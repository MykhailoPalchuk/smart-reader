//
//  WordDefinitionGameFlow.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/10/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

enum WordDefinitionGameFlow {
  static func show(in container: UIViewController, provider: WordsDefinitionProvider) {
    let gameController = WordDefinitionViewController(gameProvider: provider)
    let navigationController = UINavigationController(rootViewController: gameController)
    gameController.showResultsAction = { score, results in
      let resultsController = WordDefinitionGameResultsController(score: score, results: results)
      resultsController.completion = {
        navigationController.dismiss(animated: true, completion: nil)
      }
      navigationController.pushViewController(resultsController, animated: true)
    }
    gameController.completion = {
      navigationController.dismiss(animated: true, completion: nil)
    }
    container.present(navigationController, animated: true, completion: nil)
  }
}
