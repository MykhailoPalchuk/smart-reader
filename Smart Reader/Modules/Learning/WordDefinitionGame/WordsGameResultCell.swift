//
//  WordsGameResultCell.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/11/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

final class WordsGameResultCell: UITableViewCell, NibInstantiatable, ReusableCell {

  @IBOutlet private weak var resultImageView: UIImageView!
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var definitionLabel: UILabel!

  func render(result: VerifiedAnswer) {
    resultImageView.image = result.isCorrect ? #imageLiteral(resourceName: "right") : #imageLiteral(resourceName: "wrong")
    titleLabel.text = result.word
    definitionLabel.text = result.definition
  }
}
