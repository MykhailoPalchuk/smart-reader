//
//  WordDefinitionGameResultsController.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/11/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

final class WordDefinitionGameResultsController: UIViewController {
  var completion: (() -> Void)?

  private let score: Int
  private let results: [VerifiedAnswer]

  private lazy var closeButton: UIBarButtonItem = {
    let button = UIBarButtonItem(
      image: #imageLiteral(resourceName: "close"),
      style: .plain,
      target: self,
      action: #selector(handleCloseButtonTap)
    )
    button.tintColor = .blue
    return button
  }()
  private lazy var headerView: UIView = {
    let scoreLabel = UILabel()
    scoreLabel.text = "\(score)/\(results.count)"
    scoreLabel.font = UIFont.systemFont(ofSize: 24, weight: .semibold)
    scoreLabel.translatesAutoresizingMaskIntoConstraints = false
    let header = UIView()
    header.addSubview(scoreLabel)
    NSLayoutConstraint.activate([
      scoreLabel.centerXAnchor.constraint(equalTo: header.centerXAnchor),
      scoreLabel.centerYAnchor.constraint(equalTo: header.centerYAnchor)
    ])
    return header
  }()
  private lazy var tableView: UITableView = {
    let tableView = UITableView(frame: .zero, style: .plain)
    tableView.separatorStyle = .none
    tableView.dataSource = self
    tableView.allowsSelection = false
    tableView.register(
      WordsGameResultCell.nib,
      forCellReuseIdentifier: WordsGameResultCell.reuseIdentifier
    )
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.tableHeaderView = headerView
    tableView.clipsToBounds = false
    return tableView
  }()

  init(score: Int, results: [VerifiedAnswer]) {
    self.score = score
    self.results = results
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }

  private func setup() {
    view.backgroundColor = .white

    title = "Results"

    navigationItem.leftBarButtonItem = UIBarButtonItem(
      image: #imageLiteral(resourceName: "close"),
      style: .plain,
      target: self,
      action: #selector(handleCloseButtonTap)
    )

    view.addSubview(tableView)
    NSLayoutConstraint.activate([
      tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
      tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
    ])
  }

  @objc private func handleCloseButtonTap() {
    completion?()
  }
}

extension WordDefinitionGameResultsController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return results.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withType: WordsGameResultCell.self, for: indexPath)
    cell.render(result: results[indexPath.row])
    return cell
  }
}
