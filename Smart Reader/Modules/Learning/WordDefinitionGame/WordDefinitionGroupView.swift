//
//  WordDefinitionGroupView.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/9/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

final class WordDefinitionGroupView: UIView {
  var connectedPairs: [(String, String)] {
    return bulletPairs.map({ pair in
      let word = (wordsStack.arrangedSubviews[pair.0.tag] as? UILabel)?.text
      let definition = (definitionsStack.arrangedSubviews[pair.1.tag] as? UITextView)?.text
      return (word ?? "", definition ?? "")
    })
  }

  private let words: [String]
  private let definitions: [String]

  private var bulletPairs: [(FillableBulletView, FillableBulletView)] = []
  private var selectedWordBullet: FillableBulletView?
  private var selectedDefinitionBullet: FillableBulletView?
  private var linesView = UIView()

  private(set) var isPresenting = false

  private lazy var wordsStack: UIStackView = {
    let labels: [UILabel] = words.map { str in
      let label = UILabel()
      label.text = str
      label.textAlignment = .left
      label.numberOfLines = 1
      label.adjustsFontSizeToFitWidth = true
      return label
    }
    let stack = stackView(with: labels)
    return stack
  }()
  private lazy var wordsBulletsStack: UIStackView = {
    return bulletsStackView(count: words.count)
  }()
  private lazy var definitionsStack: UIStackView = {
    let textViews: [UITextView] = definitions.map { str in
      let textView = UITextView()
      textView.text = str
      textView.textAlignment = .left
      textView.layer.borderColor = UIColor.black.cgColor
      textView.layer.borderWidth = 1
      textView.isEditable = false
      return textView
    }
    let stack = stackView(with: textViews)
    stack.distribution = .fillEqually
    return stack
  }()
  private lazy var definitionsBulletsStack: UIStackView = {
    return bulletsStackView(count: definitions.count)
  }()
  init(words: [String], definitions: [String]) {
    self.words = words
    self.definitions = definitions

    super.init(frame: .zero)

    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func layoutSubviews() {
    super.layoutSubviews()

    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
      self.renderBulletLines()
    }
  }

  func startPresentation() {
    isPresenting = true
    isUserInteractionEnabled = false
    restartPresentation()
  }

  func endPresentation() {
    isPresenting = false
    isUserInteractionEnabled = true
  }

  private func stackView(with subviews: [UIView]) -> UIStackView {
    let stackView = UIStackView(arrangedSubviews: subviews)
    stackView.axis = .vertical
    stackView.spacing = 16
    stackView.distribution = .equalSpacing
    stackView.translatesAutoresizingMaskIntoConstraints = false
    return stackView
  }

  private func bulletsStackView(count: Int) -> UIStackView {
    var bullets = [UIView]()
    for index in 0..<count {
      let view = FillableBulletView()
      view.tag = index
      view.addGestureRecognizer(UITapGestureRecognizer(
        target: self,
        action: #selector(handleBulletTap(_:))
      ))
      bullets.append(view)
    }
    let stack = stackView(with: bullets)
    bullets.forEach { view in
      view.translatesAutoresizingMaskIntoConstraints = false
      view.widthAnchor.constraint(equalToConstant: 36).isActive = true
      view.heightAnchor.constraint(equalToConstant: 36).isActive = true
    }
    return stack
  }

  private func setup() {
    let leftStack = UIStackView(arrangedSubviews: [wordsStack, wordsBulletsStack])
    leftStack.axis = .horizontal
    leftStack.spacing = 8
    leftStack.translatesAutoresizingMaskIntoConstraints = false
    addSubview(leftStack)
    NSLayoutConstraint.activate([
      leftStack.leadingAnchor.constraint(equalTo: leadingAnchor),
      leftStack.topAnchor.constraint(equalTo: topAnchor),
      leftStack.bottomAnchor.constraint(equalTo: bottomAnchor),
      leftStack.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.3)
    ])

    let rightStack = UIStackView(arrangedSubviews: [definitionsBulletsStack, definitionsStack])
    rightStack.axis = .horizontal
    rightStack.spacing = 8
    rightStack.translatesAutoresizingMaskIntoConstraints = false
    addSubview(rightStack)
    NSLayoutConstraint.activate([
      rightStack.topAnchor.constraint(equalTo: topAnchor),
      rightStack.trailingAnchor.constraint(equalTo: trailingAnchor),
      rightStack.bottomAnchor.constraint(equalTo: bottomAnchor),
      rightStack.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5)
    ])

    linesView.clipsToBounds = true
    linesView.translatesAutoresizingMaskIntoConstraints = false
    insertSubview(linesView, at: 0)
    NSLayoutConstraint.activate([
      linesView.leadingAnchor.constraint(equalTo: wordsBulletsStack.leadingAnchor),
      linesView.topAnchor.constraint(equalTo: topAnchor),
      linesView.trailingAnchor.constraint(equalTo: definitionsBulletsStack.trailingAnchor),
      linesView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ])
  }

  @objc private func handleBulletTap(_ recognizer: UIGestureRecognizer) {
    if let bullet = recognizer.view as? FillableBulletView {
      if wordsBulletsStack.arrangedSubviews.contains(bullet) {
        handleWordBulletTap(bullet)
      } else {
        handleDefinitionBulletTap(bullet)
      }
    }
  }

  private func handleWordBulletTap(_ bullet: FillableBulletView) {
    switch bullet.state {
    case .none:
      selectedWordBullet?.renderState(.none)
      if let oposite = selectedDefinitionBullet {
        bullet.renderState(.full)
        oposite.renderState(.full)
        bulletPairs.append((bullet, oposite))
        selectedWordBullet = nil
        selectedDefinitionBullet = nil
        renderBulletLines()
      } else {
        bullet.renderState(.medium)
        selectedWordBullet = bullet
      }

    case .medium:
      bullet.renderState(.none)
      selectedWordBullet = nil

    case .full:
      if let pairIndex = bulletPairs.firstIndex(where: { $0.0 == bullet }) {
        bulletPairs[pairIndex].1.renderState(.none)
        bulletPairs.remove(at: pairIndex)
        renderBulletLines()
      }

      if let oposite = selectedDefinitionBullet {
        oposite.renderState(.full)
        bulletPairs.append((bullet, oposite))
        selectedWordBullet = nil
        selectedDefinitionBullet = nil
        renderBulletLines()
      } else {
        bullet.renderState(.none)
      }
    }
  }

  private func handleDefinitionBulletTap(_ bullet: FillableBulletView) {
    switch bullet.state {
    case .none:
      selectedDefinitionBullet?.renderState(.none)
      if let oposite = selectedWordBullet {
        bullet.renderState(.full)
        oposite.renderState(.full)
        bulletPairs.append((oposite, bullet))
        selectedWordBullet = nil
        selectedDefinitionBullet = nil
        renderBulletLines()
      } else {
        bullet.renderState(.medium)
        selectedDefinitionBullet = bullet
      }

    case .medium:
      bullet.renderState(.none)
      selectedDefinitionBullet = nil

    case .full:
      if let pairIndex = bulletPairs.firstIndex(where: { $0.1 == bullet }) {
        bulletPairs[pairIndex].0.renderState(.none)
        bulletPairs.remove(at: pairIndex)
        renderBulletLines()
      }

      if let oposite = selectedWordBullet {
        oposite.renderState(.full)
        bulletPairs.append((oposite, bullet))
        selectedWordBullet = nil
        selectedDefinitionBullet = nil
        renderBulletLines()
      } else {
        bullet.renderState(.none)
      }
    }
  }

  private func renderBulletLines() {
    linesView.subviews.first?.removeFromSuperview()
    let bulletPoints: [(Int, Int)] = bulletPairs.map {(
      wordsBulletsStack.arrangedSubviews.firstIndex(of: $0.0)!,
      definitionsBulletsStack.arrangedSubviews.firstIndex(of: $0.1)!
    )}
    let view = LinesDrawingView(
      frame: CGRect(
        x: 0,
        y: 0,
        width: linesView.frame.width,
        height: linesView.frame.height
      ),
      points: bulletPoints,
      yPoints: wordsBulletsStack.arrangedSubviews.map { $0.frame.midY }
    )
    linesView.addSubview(view)
  }

  private func connectFirstPair() {
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
      guard let self = self else {
        return
      }
      self.handleWordBulletTap(self.wordsBulletsStack.arrangedSubviews[2] as! FillableBulletView)
      DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
        guard let self = self else {
          return
        }
        self.handleDefinitionBulletTap(self.definitionsBulletsStack.arrangedSubviews[0] as! FillableBulletView)
        if self.isPresenting {
          self.connectSecondPair()
        }
      }
    }
  }

  private func connectSecondPair() {
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
      guard let self = self else {
        return
      }
      self.handleWordBulletTap(self.wordsBulletsStack.arrangedSubviews[0] as! FillableBulletView)
      DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
        guard let self = self else {
          return
        }
        self.handleDefinitionBulletTap(self.definitionsBulletsStack.arrangedSubviews[3] as! FillableBulletView)
        if self.isPresenting {
          self.connectThirdPair()
        }
      }
    }
  }

  private func connectThirdPair() {
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
      guard let self = self else {
        return
      }
      self.handleWordBulletTap(self.wordsBulletsStack.arrangedSubviews[1] as! FillableBulletView)
      DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
        guard let self = self else {
          return
        }
        self.handleDefinitionBulletTap(self.definitionsBulletsStack.arrangedSubviews[1] as! FillableBulletView)
        if self.isPresenting {
          self.connectForthPair()
        }
      }
    }
  }

  private func connectForthPair() {
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
      guard let self = self else {
        return
      }
      self.handleWordBulletTap(self.wordsBulletsStack.arrangedSubviews[3] as! FillableBulletView)
      DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
        guard let self = self else {
          return
        }
        self.handleDefinitionBulletTap(self.definitionsBulletsStack.arrangedSubviews[2] as! FillableBulletView)
        if self.isPresenting {
          self.restartPresentation()
        }
      }
    }
  }

  private func restartPresentation() {
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
      if self?.isPresenting == true {
        self?.bulletPairs = []
        self?.renderBulletLines()
        self?.unselectAllBullets()
        self?.connectFirstPair()
      }
    }
  }

  private func unselectAllBullets() {
    for bullet in wordsBulletsStack.arrangedSubviews {
      (bullet as? FillableBulletView)?.renderState(.none)
    }
    for bullet in definitionsBulletsStack.arrangedSubviews {
      (bullet as? FillableBulletView)?.renderState(.none)
    }
  }
}
