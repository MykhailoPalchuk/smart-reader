//
//  FIllableBulletView.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/10/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

final class FillableBulletView: UIView {
  enum FillState {
    case none
    case medium
    case full
  }

  private(set) var state: FillState = .none
  private let bullet = UIImageView()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  func renderState(_ state: FillState) {
    self.state = state
    switch state {
    case .none:
      bullet.image = #imageLiteral(resourceName: "bullet")

    case .medium:
      bullet.image = #imageLiteral(resourceName: "bullet_medium_fill")

    case .full:
      bullet.image = #imageLiteral(resourceName: "bullet_full_fill")
    }
  }

  private func setup() {
    bullet.tintColor = .blue
    bullet.translatesAutoresizingMaskIntoConstraints = false
    addSubview(bullet)
    NSLayoutConstraint.activate([
      bullet.centerXAnchor.constraint(equalTo: centerXAnchor),
      bullet.centerYAnchor.constraint(equalTo: centerYAnchor),
      bullet.widthAnchor.constraint(equalToConstant: 24),
      bullet.heightAnchor.constraint(equalToConstant: 24)
      ])
    renderState(.none)
  }
}
