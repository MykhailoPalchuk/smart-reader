//
//  LinesDrawingView.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/10/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

final class LinesDrawingView: UIView {
  let yPoints: [CGFloat]
  let points: [(Int, Int)]

  init(frame: CGRect, points: [(Int, Int)], yPoints: [CGFloat]) {
    self.points = points
    self.yPoints = yPoints
    super.init(frame: frame)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func draw(_ rect: CGRect) {
    super.draw(rect)

    var leftPoints: [CGPoint] = []
    var rightPoints: [CGPoint] = []
    for y in yPoints {
      leftPoints.append(CGPoint(x: 18, y: y))
      rightPoints.append(CGPoint(x: rect.width - 18, y: y))
    }

    let offset: CGFloat = 5
    for (a, b) in points {
      let path = UIBezierPath()
      path.move(to: CGPoint(x: leftPoints[a].x, y: leftPoints[a].y - offset))
      path.addLine(to: CGPoint(x: rightPoints[b].x, y: rightPoints[b].y - offset))
      path.addLine(to: CGPoint(x: rightPoints[b].x, y: rightPoints[b].y + offset))
      path.addLine(to: CGPoint(x: leftPoints[a].x, y: leftPoints[a].y + offset))
      path.addLine(to: CGPoint(x: leftPoints[a].x, y: leftPoints[a].y - offset))
      path.close()

      let sublayer = CAShapeLayer()
      sublayer.strokeColor = UIColor.blue.cgColor
      sublayer.fillColor = UIColor.blue.cgColor
      sublayer.path = path.cgPath
      layer.addSublayer(sublayer)
    }
  }
}
