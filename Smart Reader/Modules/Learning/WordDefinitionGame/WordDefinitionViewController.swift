//
//  WordDefinitionViewController.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/9/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

final class WordDefinitionViewController: UIViewController {
  var showResultsAction: ((Int, [VerifiedAnswer]) -> Void)?
  var completion: (() -> Void)?

  private let gameProvider: WordsDefinitionProvider

  private lazy var closeButton: UIBarButtonItem = {
    let button = UIBarButtonItem(
      image: #imageLiteral(resourceName: "close"),
      style: .plain,
      target: self,
      action: #selector(handleCloseButtonTap)
    )
    button.tintColor = .blue
    return button
  }()
  private var groupViewContainer = UIView()
  private lazy var nextButton: UIButton = {
    let button = UIButton()
    button.setTitle("Next", for: .normal)
    button.setTitleColor(.blue, for: .normal)
    button.layer.cornerRadius = 17
    button.clipsToBounds = true
    button.layer.borderColor = UIColor.blue.cgColor
    button.layer.borderWidth = 1
    button.addTarget(
      self,
      action: #selector(handleNextButtonTap),
      for: .touchUpInside
    )
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()

  init(gameProvider: WordsDefinitionProvider) {
    self.gameProvider = gameProvider
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }

  private func setup() {
    view.backgroundColor = .white

    navigationItem.leftBarButtonItem = UIBarButtonItem(
      image: #imageLiteral(resourceName: "close"),
      style: .plain,
      target: self,
      action: #selector(handleCloseButtonTap)
    )

    view.addSubview(nextButton)
    NSLayoutConstraint.activate([
      nextButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      nextButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8),
      nextButton.heightAnchor.constraint(equalToConstant: 40),
      nextButton.widthAnchor.constraint(equalToConstant: 142)
    ])

    groupViewContainer.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(groupViewContainer)
    NSLayoutConstraint.activate([
      groupViewContainer.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
      groupViewContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
      groupViewContainer.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
      groupViewContainer.bottomAnchor.constraint(equalTo: nextButton.topAnchor, constant: -40)
    ])

    showNextGamePack()
  }

  @objc private func handleCloseButtonTap() {
    completion?()
  }

  @objc private func handleNextButtonTap() {
    if let groupView = groupViewContainer.subviews.first as? WordDefinitionGroupView {
      gameProvider.acceptAnswer(groupView.connectedPairs)
    }
    showNextGamePack()
  }

  private func showNextGamePack() {
    gameProvider.getNextDataPack { [weak self] pack in
      guard let self = self else {
        return
      }

      guard let pack = pack  else {
          self.showResults()
          return
      }

      let groupView = WordDefinitionGroupView(words: pack.words, definitions: pack.definitions.rearranged())
      groupView.translatesAutoresizingMaskIntoConstraints = false
      self.showNextGroupView(groupView)
    }
  }

  private func showNextGroupView(_ groupView: UIView) {
    if let previousView = groupViewContainer.subviews.first {
      previousView.removeFromSuperview()
    }

    groupViewContainer.addSubview(groupView)
    NSLayoutConstraint.activate([
      groupView.leadingAnchor.constraint(equalTo: groupViewContainer.leadingAnchor),
      groupView.topAnchor.constraint(equalTo: groupViewContainer.topAnchor),
      groupView.trailingAnchor.constraint(equalTo: groupViewContainer.trailingAnchor),
      groupView.bottomAnchor.constraint(equalTo: groupViewContainer.bottomAnchor)
    ])
  }

  private func showResults() {
    showResultsAction?(gameProvider.score, gameProvider.results)
  }
}
