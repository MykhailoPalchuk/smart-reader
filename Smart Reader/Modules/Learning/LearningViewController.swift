//
//  LearningViewController.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 5/5/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

final class LearningViewController: UIViewController {
  let provider = CachedDefinitionsProvider()

  private lazy var gameTypeLabel: UILabel = {
    let label = UILabel()
    label.text = "Choose a words set to play with"
    label.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  private lazy var buttonsStackView: UIStackView = {
    let stackView = UIStackView()
    stackView.axis = .horizontal
    stackView.spacing = 16
    stackView.distribution = .fillEqually
    stackView.alignment = .center
    stackView.translatesAutoresizingMaskIntoConstraints = false
    return stackView
  }()
  private lazy var playRandomButton: UIButton = {
    let button = UIButton()
    if WordsCache.shared.words.count > 4 {
      button.setTitle("Random words", for: .normal)
    } else {
      button.setTitle("Start", for: .normal)
    }
    button.setTitleColor(.blue, for: .normal)
    button.layer.cornerRadius = 17
    button.clipsToBounds = true
    button.layer.borderColor = UIColor.blue.cgColor
    button.layer.borderWidth = 1
    button.addTarget(
      self,
      action: #selector(handleRandomButtonTap),
      for: .touchUpInside
    )
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  private lazy var playCachedButton: UIButton = {
    let button = UIButton()
    button.setTitle("Known words", for: .normal)
    button.setTitleColor(.blue, for: .normal)
    button.layer.cornerRadius = 17
    button.clipsToBounds = true
    button.layer.borderColor = UIColor.blue.cgColor
    button.layer.borderWidth = 1
    button.addTarget(
      self,
      action: #selector(handleChachedButtonTap),
      for: .touchUpInside
    )
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()
  private lazy var groupView: WordDefinitionGroupView = {
    let words = ["life", "is", "really", "good"]
    let definitions = [
      "- used as intensifiers; `real' is sometimes used informally for `really'; `rattling' is informal",
      "- third person singular present of be.",
      "- that which is pleasing or valuable or useful",
      "- the experience of being alive; the course of human events and activities"
    ]
    let groupView = WordDefinitionGroupView(words: words, definitions: definitions)
    groupView.translatesAutoresizingMaskIntoConstraints = false
    return groupView
  }()

  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    tabBarController?.title = "Learning"
    tabBarController?.navigationItem.rightBarButtonItem = nil

    if !groupView.isPresenting {
      groupView.startPresentation()
    }
  }

  private func setup() {
    view.backgroundColor = .white

    view.addSubview(buttonsStackView)
    NSLayoutConstraint.activate([
      buttonsStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      buttonsStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8),
      buttonsStackView.heightAnchor.constraint(equalToConstant: 40)
    ])
    buttonsStackView.addArrangedSubview(playRandomButton)
    playRandomButton.widthAnchor.constraint(equalToConstant: 142).isActive = true
    if WordsCache.shared.words.count > 4 {
      buttonsStackView.addArrangedSubview(playCachedButton)

      view.addSubview(gameTypeLabel)
      NSLayoutConstraint.activate([
        gameTypeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        gameTypeLabel.bottomAnchor.constraint(equalTo: buttonsStackView.topAnchor, constant: -8)
      ])
    }

    view.addSubview(groupView)
    NSLayoutConstraint.activate([
      groupView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
      groupView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
      groupView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
      groupView.bottomAnchor.constraint(equalTo: buttonsStackView.topAnchor, constant: -40)
    ])
  }

  @objc private func handleRandomButtonTap() {

  }

  @objc private func handleChachedButtonTap() {
    WordDefinitionGameFlow.show(in: self, provider: CachedDefinitionsProvider())
  }
}
