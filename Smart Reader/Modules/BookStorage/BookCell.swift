//
//  BookCell.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 4/3/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

class BookCell: UICollectionViewCell {
  var optionsButtonTap: (() -> Void)?

  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var authorLabel: UILabel!
  @IBOutlet weak var optionsButton: UIButton!

  override func awakeFromNib() {
    super.awakeFromNib()

    optionsButton.addTarget(
      self,
      action: #selector(handleOptionsButtonAction),
      for: .touchUpInside
    )
  }

  func render(title: String, author: String) {
    titleLabel.text = title
    authorLabel.text = author
  }

  @objc private func handleOptionsButtonAction() {
    optionsButtonTap?()
  }
}
