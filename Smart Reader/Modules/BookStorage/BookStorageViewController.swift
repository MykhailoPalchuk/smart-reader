//
//  BookStorageViewController.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 4/3/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

class BookStorageViewController: UIViewController {

  // MARK: - Properties

  var picker: UIDocumentPickerViewController?
  var parser = EpubParser()

  private let store = BookStore()
  private var books: [BookModel] = []

  private lazy var addButton = UIBarButtonItem(
    barButtonSystemItem: .add,
    target: self,
    action: #selector(handleButtonTap)
  )
  private lazy var collectionView: UICollectionView = {
    let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
    view.backgroundColor = .white
    view.dataSource = self
    view.delegate = self
    return view
  }()

  // MARK: - Overrides

  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    tabBarController?.title = "Smart Reader"
    tabBarController?.navigationItem.rightBarButtonItem = addButton
  }

  // MARK: - Private

  private func setup() {
    books = store.getBooks() ?? []
    view.backgroundColor = .white

    view.addSubview(collectionView)
    collectionView.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
      collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
      collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
      collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16)
    ])
    collectionView.register(
      UINib(nibName: "BookCell", bundle: nil),
      forCellWithReuseIdentifier: "BookCell"
    )
  }

  @objc private func handleButtonTap() {
    picker = UIDocumentPickerViewController(documentTypes: ["org.idpf.epub-container"], in: .import)
    picker?.delegate = self
    present(picker!, animated: true)
  }

  private func open(book: BookModel) {
    let reader = BookReaderController(
      transitionStyle: .pageCurl,
      navigationOrientation: .horizontal,
      options: nil
    )
    reader.book = book
    reader.safeAreaSize = CGSize(
      width: view.frame.width - view.safeAreaInsets.left - view.safeAreaInsets.right,
      height: view.frame.height - view.safeAreaInsets.top - view.safeAreaInsets.bottom
    )
    present(reader, animated: true, completion: nil)
  }

  private func showOptions(for book: BookModel) {
    let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
      if let index = self.books.firstIndex(where: { $0.id == book.id }) {
        self.books.remove(at: index)
        self.store.saveBooks(self.books)
        self.collectionView.reloadData()
      }
    }))
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    present(alert, animated: true)
  }
}

// MARK: - Extension UIDocumentPickerDelegate

extension BookStorageViewController: UIDocumentPickerDelegate {
  func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
    print("Selected files at urls: \(urls)")

    guard let book = parser.parseBook(at: urls[0]) else {
      return
    }

    books.append(book)
    store.saveBooks(books)
    collectionView.reloadData()
    open(book: book)
  }
}

extension BookStorageViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return books.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard
      let cell = collectionView
          .dequeueReusableCell(withReuseIdentifier: "BookCell", for: indexPath) as? BookCell
    else {
      fatalError()
    }

    let book = books[indexPath.row]
    cell.render(title: book.title, author: book.author)
    cell.optionsButtonTap = { [weak self] in
        self?.showOptions(for: book)
    }
    return cell
  }
}

extension BookStorageViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    open(book: books[indexPath.row])
    collectionView.deselectItem(at: indexPath, animated: true)
  }
}

extension BookStorageViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: 150, height: 200)
  }
}
