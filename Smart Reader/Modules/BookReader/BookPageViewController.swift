//
//  BookPageViewController.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 2/25/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

class BookPageViewController: UIViewController, StoryboardInstantiatable {
  var text: NSAttributedString?
  var bookTitle: String?
  var pageNumber: Int?
  var turnPageLeft: (() -> Void)?
  var turnPageRight: (() -> Void)?

  @IBOutlet weak var topControlsView: UIView!
  @IBOutlet weak var backButton: UIButton!
  @IBOutlet weak var fontButton: UIButton!
  @IBOutlet weak var textView: TouchTreckingTextView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var pageNumberLabel: UILabel!
  
  private var additionalView: UIView?

  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    if let text = self.text {
      textView.attributedText = text
    }

    if let page = pageNumber {
      pageNumberLabel.text = String(page)
    }
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
  }

  private func setup() {
    titleLabel.text = bookTitle
    backButton.tintColor = fontButton.titleColor(for: .normal)
    backButton.addTarget(
      self,
      action: #selector(BookPageViewController.handleBackButtonTap),
      for: .touchUpInside
    )

    textView.delegate = self
    textView.touchAction = { [weak self] touch in
      self?.handleTextViewTouch(touch)
    }
  }

  private func showWordDetails(_ word: String, range: UITextRange) {
    let container = WordInfoContainerView()
    container.render(word: word) { [weak self] error in
      guard
        error == nil,
        let self = self
      else {
        return
      }

      self.view.addSubview(container)
      container.translatesAutoresizingMaskIntoConstraints = false
      NSLayoutConstraint.activate([
        container.widthAnchor.constraint(equalToConstant: 332),
        container.heightAnchor.constraint(equalToConstant: 168),
        container.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
      ])

      if self.textView.caretRect(for: range.start).minY < self.textView.frame.height / 2 {
        container.bottomAnchor.constraint(equalTo: self.textView.bottomAnchor).isActive = true
      } else {
        container.topAnchor.constraint(equalTo: self.textView.topAnchor).isActive = true
      }
      self.additionalView = container
    }
    container.presentDetails = { [weak self] words in
      self?.additionalView?.removeFromSuperview()
      self?.additionalView = nil
      let detailsController = WordDetailsViewController(word: word, details: words)
      self?.present(detailsController, animated: true, completion: nil)
    }
  }

  private func showSelectionMenu() {
    pageNumberLabel.isHidden = true
  }

  private func hideSelectionMenu() {
    pageNumberLabel.isHidden = false
  }

  private func toggleControlsHidden() {
    topControlsView.alpha = topControlsView.alpha == 1 ? 0 : 1
  }

  private func handleTextViewTouch(_ touch: UITouch) {
    let location = touch.location(in: textView).x
    //FIX: This triggers turn of 2 pages at a time
//    if location < textView.frame.size.width / 3 {
//      turnPageLeft?()
//    } else if location > (textView.frame.size.width / 3) * 2 {
//      turnPageRight?()
//    } else {
//      toggleControlsHidden()
//    }
    if location > textView.frame.size.width / 3,
      location < (textView.frame.size.width / 3) * 2 {
      toggleControlsHidden()
    }

    additionalView?.removeFromSuperview()
    additionalView = nil
  }

  @objc private func handleBackButtonTap() {
    dismiss(animated: true, completion: nil)
  }
}

extension BookPageViewController: UITextViewDelegate {
  func textViewDidChangeSelection(_ textView: UITextView) {
    additionalView?.removeFromSuperview()
    additionalView = nil

    if let range = textView.selectedTextRange,
      let text = textView.text(in: range),
      !text.isEmpty {
      let word = String(text.split(separator: " ")[0].lowercased())
      showWordDetails(word, range: range)
    } else {
      hideSelectionMenu()
    }
  }
}
