//
//  BookReaderController.swift
//  Smart Reader
//
//  Created by Mykhailo Palchuk on 2/25/19.
//  Copyright © 2019 mpalchuk. All rights reserved.
//

import UIKit

class BookReaderController: UIPageViewController {
  var safeAreaSize: CGSize = CGSize.zero

  var contentControllers: [BookPageViewController] = []
  var book: BookModel!
  var pages: [NSAttributedString] = []
  var currentPage: Int = 0

  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    navigationController?.interactivePopGestureRecognizer?.isEnabled = true
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

    navigationController?.interactivePopGestureRecognizer?.isEnabled = false
  }

  private func setup() {
    view.backgroundColor = .white
    isDoubleSided = false

    guard let book = self.book else {
      fatalError()
    }

    dataSource = self
    let contentWidth = safeAreaSize.width - 20
    let contentHeight = safeAreaSize.height - 80
    let text = NSAttributedString(
      string: book.paragraphs.joined(separator: "\n"),
      attributes: [.font: UIFont.systemFont(ofSize: 18)]
    )
    pages = TextManager.slice(text: text, width: contentWidth, pageHeight: contentHeight)
    let controller = makePageController(text: pages[0], page: 1)
    setViewControllers(
      [controller],
      direction: .forward,
      animated: false,
      completion: nil
    )
  }

  private func turnPageLeft() {
    if let pageController = previousPage() {
      setViewControllers(
        [pageController],
        direction: .reverse,
        animated: true,
        completion: nil
      )
    }
  }

  private func turnPageRight() {
    if let pageController = nextPage() {
      setViewControllers(
        [pageController],
        direction: .forward,
        animated: true,
        completion: nil
      )
    }
  }

  private func makePageController(text: NSAttributedString, page: Int) -> BookPageViewController {
    let controller = BookPageViewController.makeStoryboardInstance()
    controller.turnPageLeft = turnPageLeft
    controller.turnPageRight = turnPageRight
    controller.text = text
    controller.bookTitle = book.title
    controller.pageNumber = page
    return controller
  }

  private func previousPage() -> BookPageViewController? {
    guard currentPage > 0 else {
      return nil
    }

    currentPage -= 1

    return makePageController(text: pages[currentPage], page: currentPage + 1)
  }

  private func nextPage() -> BookPageViewController? {
    guard currentPage < pages.count else {
      return nil
    }

    currentPage += 1

    return makePageController(text: pages[currentPage], page: currentPage + 1)
  }
}

extension BookReaderController: UIPageViewControllerDataSource {
  func pageViewController(
    _ pageViewController: UIPageViewController,
    viewControllerBefore viewController: UIViewController
    ) -> UIViewController? {
    return previousPage()
  }

  func pageViewController(
    _ pageViewController: UIPageViewController,
    viewControllerAfter viewController: UIViewController
    ) -> UIViewController? {
    return nextPage()
  }
}
